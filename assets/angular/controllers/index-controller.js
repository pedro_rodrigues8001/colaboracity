angular.module("app").controller("IndexController", function($scope, $http, $location){
	
	$scope.email = {};
	var novoEmail;
	
	$scope.sendEmail = function(){
		
		$http.get("http://colaboracity-env.elasticbeanstalk.com/rest/service/email/"+$scope.email.text)
		.success(function(){
			$(location).attr("href", "index.html")
			alert("Email cadastrado com sucesso!");
		})
		.error(function(response){
			alert("Aconteceu um erro ineseprado ao tentar cadastrar seu email");
			console.log(response);
		});
	}
})